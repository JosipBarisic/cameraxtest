package com.cinnamon.cameraxtest

import android.app.Application
import timber.log.Timber

//@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }
}