package com.cinnamon.cameraxtest.main_activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.format.DateUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.core.VideoCapture
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cinnamon.cameraxtest.R
import com.otaliastudios.transcoder.Transcoder
import com.otaliastudios.transcoder.TranscoderListener
import com.otaliastudios.transcoder.strategy.DefaultVideoStrategy
import com.otaliastudios.transcoder.strategy.size.ExactResizer
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

//@AndroidEntryPoint
@SuppressLint("RestrictedApi")
class MainActivity : AppCompatActivity() {
    private var videoCapture: VideoCapture? = null

    private lateinit var timer: Timer
    private var timePassed: Long = 0

    //    @Inject lateinit var diTester: TestDI\
    private lateinit var outputDirectory: File

    private val videoCapturedCallback = object : VideoCapture.OnVideoSavedCallback {
        override fun onVideoSaved(outputFileResults: VideoCapture.OutputFileResults) {
            Toast.makeText(this@MainActivity, "Recording Saved", Toast.LENGTH_SHORT).show()
            Timber.tag(TAG).d("onVideoSaved ${outputFileResults.savedUri?.path}")
            outputFileResults.savedUri?.path?.let {
                Timber.tag(TAG).d("transcodeVideo $it")
                transcodeVideo(it)
            }
        }

        override fun onError(videoCaptureError: Int, message: String, cause: Throwable?) {
            Toast.makeText(this@MainActivity, "Recording Failed", Toast.LENGTH_SHORT).show()
            Timber.tag(TAG).e("onError $videoCaptureError $message")
        }
    }

    private fun transcodeVideo(filePath: String) {
        val videoStrategy = DefaultVideoStrategy.Builder()
            .addResizer(ExactResizer(1080, 1920))
            .frameRate(DEFAULT_FPS)
            .build()
        val videoTranscoder = Transcoder.into(filePath).setListener(object : TranscoderListener {
            override fun onTranscodeProgress(progress: Double) {
                Timber.tag(TAG).d("onTranscodeProgress: $progress")
                runOnUiThread {
                    progress_tv.text =
                        getString(R.string.percentage, "${(progress * 100).toInt()}%")
                }
            }

            override fun onTranscodeCompleted(successCode: Int) {
                Timber.tag(TAG)
                    .d("onTranscodeCompleted: success -> ${successCode == Transcoder.SUCCESS_TRANSCODED}")
                when (successCode) {
                    Transcoder.SUCCESS_TRANSCODED -> runOnUiThread {
                        progress_tv.text = getString(R.string.hundred_percent)
                        Handler(Looper.getMainLooper()).postDelayed({
                            progress_tv.text = getString(R.string.transcoding_success)
                        }, 500)
                        Handler(Looper.getMainLooper()).postDelayed({
                            progress_tv.text = ""
                        }, 1500)
                    }
                    else -> runOnUiThread {
                        progress_tv.text = getString(R.string.transcoding_failed)
                    }
                }
            }

            override fun onTranscodeCanceled() {
                Timber.tag(TAG).d("onTranscodeCanceled")
            }

            override fun onTranscodeFailed(exception: Throwable) {
                Timber.tag(TAG).d("onTranscodeFailed: $exception")
            }
        }).addDataSource(filePath).setVideoTrackStrategy(videoStrategy)

        try {
            videoTranscoder.transcode()
        } catch (e: IOException) {
            Timber.tag(TAG).e(e)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        diTester.helloWorld()

        // Request camera permissions
        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
            )
        }

        // Set up the listener for take photo button
        camera_start.setOnClickListener { recordVideo() }
        camera_stop.setOnClickListener { stopRecordingVideo() }

        outputDirectory = getOutputDirectory()
        timer = Timer()
    }

    private fun recordVideo() {
        val videoFile = File(
            outputDirectory,
            SimpleDateFormat(
                FILENAME_FORMAT, Locale.US
            ).format(System.currentTimeMillis()) + ".mp4"
        )

        val outputFileOptions = VideoCapture.OutputFileOptions.Builder(videoFile).build()

        val videoCapture = videoCapture ?: return


        Timber.tag(TAG).d("recordVideo: ${videoCapture.attachedSurfaceResolution}")
        Timber.tag(TAG).d(
            "recordVideo def_conf: ${videoCapture.currentConfig.defaultCaptureConfig.implementationOptions}"
        )
        Timber.tag(TAG).d("recordVideo outputOptions: $outputFileOptions")
        Timber.tag(TAG).d("recordVideo cam: ${videoCapture.camera?.cameraInfo?.implementationType}")
        Timber.tag(TAG).d(
            "recordVideo cam_internal: ${videoCapture.camera?.cameraInfoInternal?.implementationType}"
        )

        timePassed = 0
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    time.text = DateUtils.formatElapsedTime(timePassed).toString()
                }
                timePassed++
            }
        }, 0, 1000)

        videoCapture.startRecording(
            outputFileOptions,
            ContextCompat.getMainExecutor(this),
            videoCapturedCallback
        )
    }

    private fun stopRecordingVideo() {
        timer.cancel()
        timer = Timer()
        time.text = ""
        videoCapture?.stopRecording()
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(applicationContext)

        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()

            /*val camera2Interop = Camera2Interop.Extender(preview)
            camera2Interop.setCaptureRequestOption(
                CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE,
                Range(DEFAULT_FPS, DEFAULT_FPS)
            )*/

            val videoUseCase = preview.build().apply {
                setSurfaceProvider(preview_view.surfaceProvider)
            }

            videoCapture = VideoCapture.Builder().apply {
                setBitRate(DEFAULT_BIT_RATE)
            }.build()

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, videoUseCase, videoCapture
                )

            } catch (exc: Exception) {
                Timber.tag(TAG).e("Use case binding failed -> $exc")
            }

        }, ContextCompat.getMainExecutor(this))
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun getOutputDirectory(): File {
        val mediaDir = externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else filesDir
    }

    companion object {
        private const val TAG = "CameraXBasic"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val DEFAULT_BIT_RATE = 20000000
        private const val DEFAULT_FPS = 15
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS =
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults:
        IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(
                    this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }
}